# Build
FROM gcc:11 as builder

RUN mkdir -p /usr/word_calc
COPY . /usr/word_calc/

RUN cd /usr/word_calc; \ 
    cd src; \
    g++ main.cpp -o main
RUN cd /usr/word_calc; ls 
RUN cd /usr/word_calc/src; ls

RUN apt-get update -y && \
    apt-get upgrade -y && \
    apt-get dist-upgrade -y

WORKDIR /usr/word_calc/src

ENTRYPOINT ["/usr/word_calc/src"]
