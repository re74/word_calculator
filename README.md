
# Word Calculator

The program calculates result of a mathematical expression given in words.
\
The expression must be given according to the following **rules**:
- The expression must consist of **two** operands and one **operation** (**minus** before the operands is accepted).
---
   ✔️ minus one plus seventy-five
   \
   ❌ one seventy-five 

---  
- Available operations are **over**, **times**, **minus**, **plus**.
---
   ✔️ one times sixty
   \
   ✔️ six over seventy-five
   \
   ✔️ one plus sixty
   \
   ✔️ six minus seventy-five
   \
   ❌ one multiplied by sixty
   \
   ❌ six divided by seventy-five
   \
   ❌ one add sixty
   \
   ❌ six subtract seventy-five
   
---
- The **maximum** available operand is **nine hundred ninty-nine**.
- The **minimum** available operand is **minus nine hundred ninety-nine**.
- The **hundred** has to always have a digit in front of it and no **and** after it.
---
   ✔️ minus one hundred times seven hundred fifty-two
   \
   ❌ minus hundred times seven hundred and fifty-two

---
- The **-ty** word has to be connected to the following digit (if there is one) with the **dash**
---
   ✔️ minus eighty-one plus two
   \
   ❌ minus eighty one plus two

--- 
- The input has to start with a letter and finish after the last letter.
- The input can not start with an operation, unless it is **minus**.
---
   ✔️ minus ten times minus three
   \
   ❌ plus ten times minus three

---
- The expression should be a common-sense expression.
- The exression is case-insensitive.
---
   ✔️ nine hundred ninety over minus two hundred eleven
   \
   ✔️ Nineteen plus zero
   \
   ❌ thirty-eleven over sixteen hundred
   \
   ❌ one thirty-five plus over six

---


## Usage

1. Clone the repository.
Inside the src folder run
```sh
g++ main.cpp -o main
./main
```
2. When prompted, enter the expression. 
3. Observe the output.
4. To quit, press q or Q.
\
 *Output example*
```sh
Enter your expression or Q to exit: nine times zero

The answer is 0
Enter your expression or Q to exit: Eleven over minus two

The answer is -5.5
Enter your expression or Q to exit: minus Seven hundred seventy-seven times minus sixty-six   

The answer is 4662
Enter your expression or Q to exit: one hundred ten plus zero

The answer is 110
Enter your expression or Q to exit: twenty times fifty

The answer is 1000
Enter your expression or Q to exit: one minus hundred

Could not parse your input. Make sure the input follows the rules.
Enter your expression or Q to exit: one minus one hundred

The answer is -99
Enter your expression or Q to exit: q
Bye! 

```


## Maintainers

@MariaNema
