#include <iostream>
#include <string>
#include <vector>
#include <map>

class Word_Calculator
{
    double operands[2]={0.0,0.0};
    double operands_signs[2]={1.0,1.0};
    int operation = 0;
    double result = 0.0;

    enum oper {division, multiplication, addition, subtraction};
    void Add () { result = operands[0] + operands[1]; }
    void Subtract () { result = operands[0] - operands[1]; }      
    void Multiplicate () { result = operands[0] * operands[1]; }     
    void Divide () 
    { 
        if (operands[1])
            result = operands[0] / operands[1]; 
        else 
            throw "Division by 0";
    } 
    /*
    *@desc Parses input ( updates operands, their signs, and operation)
    *@param s User input
    *@returns true in case the parsing went well, and false otherwise.
    */
    bool Parse_input(std::string s)
    { 
        std::map<std::string, int> digit_map; // maps single digit-words to their values
        digit_map.insert(std::make_pair("zero",0.0));
        digit_map.insert(std::make_pair("one",1.0));
        digit_map.insert(std::make_pair("two",2.0));
        digit_map.insert(std::make_pair("three",3.0));
        digit_map.insert(std::make_pair("four",4.0));
        digit_map.insert(std::make_pair("five",5.0));
        digit_map.insert(std::make_pair("six",6.0));
        digit_map.insert(std::make_pair("seven",7.0));
        digit_map.insert(std::make_pair("eight",8.0));
        digit_map.insert(std::make_pair("nine",9.0));
        std::map<std::string, int> teen_map; // maps -teen words to their values
        teen_map.insert(std::make_pair("ten",10.0));
        teen_map.insert(std::make_pair("eleven",11.0));
        teen_map.insert(std::make_pair("twelve",12.0));
        teen_map.insert(std::make_pair("thirteen",13.0));
        teen_map.insert(std::make_pair("fourteen",14.0));
        teen_map.insert(std::make_pair("fifteen",15.0));
        teen_map.insert(std::make_pair("sixteen",16.0));
        teen_map.insert(std::make_pair("seventeen",17.0));
        teen_map.insert(std::make_pair("eighteen",18.0));
        teen_map.insert(std::make_pair("nineteen",19.0));
        std::map<std::string, int> ty_map; // maps -ty words to their values
        ty_map.insert(std::make_pair("twenty",20.0));
        ty_map.insert(std::make_pair("thirty",30.0));
        ty_map.insert(std::make_pair("forty",40.0));
        ty_map.insert(std::make_pair("fifty",50.0));
        ty_map.insert(std::make_pair("sixty",60.0));
        ty_map.insert(std::make_pair("seventy",70.0));
        ty_map.insert(std::make_pair("eighty",80.0));
        ty_map.insert(std::make_pair("ninety",90.0));
        std::map<std::string, int> op_map; // maps operations
        op_map.insert(std::make_pair("plus", oper::addition));
        op_map.insert(std::make_pair("minus", oper::subtraction));
        op_map.insert(std::make_pair("times", oper::multiplication));
        op_map.insert(std::make_pair("over", oper::division));

        enum expr_blocks { digit, teen, ty, hundred, dash, op }; // expression blocks (dash is the dash in such a words as e.g. "thirty-one")
        std::vector <int> order={}; // holds the order of expression blocks, such as "digit, teen, ty, hundred, dash, op"

        int current_operand=0; // idx of current operand       
        bool is_dash=false;       
        std::string word= "";
        s=s+' '; // for convinience in for-loop
        int input_length=s.length(); 
        //parse the words.
        for (int i=0; i < input_length; i++ )
        {
            char letter = std::tolower(s[i]);
            // if the letter is the dash or gap - analyze the word before it.
            if ((is_dash = (letter == '-')) || letter == ' ')           
            {                            
                if (digit_map.find(word) != digit_map.end()) //the word is a digit                
                {
                    // check the order: digit can not go after digit, -teen, or -ty
                    if (!order.empty() && (order.back() == expr_blocks::digit || order.back()  == expr_blocks::teen || order.back()  == expr_blocks::ty)) { return false; }   
                    order.push_back(expr_blocks::digit);                       
                    operands[current_operand]+=digit_map.at(word);       
                }

                else if (teen_map.find(word) != teen_map.end()) // the word is a -teen word (from ten to nineteen)
                {
                    // check the order: -teen can not go after dash, digit, -teen or -ty.
                    if (!order.empty() && (order.back()  == expr_blocks::dash || order.back()  == expr_blocks::digit || order.back()  == expr_blocks::teen || order.back()  == expr_blocks::ty)) { return false; }
                    order.push_back(expr_blocks::teen);                    
                    operands[current_operand]+=teen_map.at(word);
                }    

                else if (ty_map.find(word) != ty_map.end()) // the word is a -ty word (twenty, thirty and so on)
                {
                    // check the order: -ty can not go after dash, digit, -teen or -ty.
                    if (!order.empty()  && (order.back()  == expr_blocks::dash || order.back()  == expr_blocks::digit || order.back()  == expr_blocks::teen || order.back()  == expr_blocks::ty) ) { return false; }
                    order.push_back(expr_blocks::ty);                    
                    operands[current_operand]+=ty_map.at(word);                    
                }
                
                else if (op_map.find(word) != op_map.end()) // the word is an operation
                {
                    // check the order: the operation can not go after dash
                    if (!order.empty() && order.back()  == expr_blocks::dash) { return false; }  
                    //if operation is placed first or after another operation  
                    if (order.size()==0 || order.back() == expr_blocks::op)
                    {
                        if (op_map.at(word) == oper::subtraction) //if operation is minus
                            //update the sign of the operand
                            operands_signs[current_operand]*=-1; 
                        else
                            return false;
                    }
                    else
                    {
                        order.push_back(expr_blocks::op);
                        //continue parsing to the next operand 
                        current_operand++;
                        // there can only be two operands
                        if (current_operand > 1) { return false; }
                        operation = op_map.at(word);     
                    }                          
                }

                else if (word=="hundred") // the word is hundred                               
                {
                    // check the order: the  hundred goes after digit
                    if (order.empty() || order.back() != expr_blocks::digit ) { return false; }
                    order.push_back(expr_blocks::hundred);
                    if (operands[current_operand])
                        operands[current_operand]*=100.0;
                }

                else // the word is not recognized
                    return false; 

                if (is_dash) // if there is a dash
                {
                    // check the order: the dash goes after -ty
                    if (order.empty() || order.back() != expr_blocks::ty ) { return false; }
                    order.push_back(expr_blocks::dash);   
                } 
                // zero the word 
                word="";               
            }

            else // if not the end of the word
                word = word + letter;            
        }  
        // check: there should be one operation (if there is none - the second operand is not parsed) 
        if (!current_operand) { return false; }
        
        // check the order: the last part is not operation or dash
        if (order.back() == expr_blocks::op || order.back() == expr_blocks::dash) { return false; }
         
        return true;         
    }

    /*
    *@desc Zeroes the operands, operation, and result, refreshes operabds' signs.    
    */
    void Refresh_calculator ()
    {
            operands[0]=0.0;
            operands[1]=0.0;
            operands_signs[0]=1.0;
            operands_signs[1]=1.0;
            operation = 0;
            result = 0.0;
    }
    public: 
        void Calculate (std::string s)
        {
            Refresh_calculator();
            if (Parse_input(s))
            {     
                //Calculate operands's signs
                operands[0]*=operands_signs[0];
                operands[1]*=operands_signs[1];
                try
                {
                    switch (operation)
                    {
                        case oper::division:
                            Divide();
                            break;
                        case oper::multiplication:
                            Multiplicate();
                            break;
                        case oper::addition:
                            Add();
                            break;
                        case oper::subtraction:
                            Subtract();
                            break;
                    }
                }
                catch (const char * ex)
                {
                    std::cout <<std::endl << "An error occured: " << ex << std::endl;
                    return;
                } 
                std::cout <<std::endl << "The answer is "<< result <<std::endl;           
            }
            else 
                std::cout <<std::endl << "Could not parse your input. Make sure the input follows the rules."<<std::endl;  
        }
};

/*
*@desc The program is aimed to calculate mathematical expression 
    that consists of two operands and one operation.
    The expression is a word sequence. 
    Available operations are "over", "times", "minus", "plus".
    The maximum available operand is "nine hundred ninty-nine".
    The minimum available operand is "minus nine hundred ninety-nine".
    The "hundred" has to always have a digit in front of it, 
    e.g. "one hundred", "seven hundred". 
    The -ty words has to be connected to following digits (if there are any) with dash, 
    e.g. "thirty-nine", "sixty-four".
    The input has to start with a letter and finish after the last letter.
    The input can not start with an operation, unless it is "minus".
    The input should build a common-sense expression.
*/
int main()
{
    Word_Calculator word_calculator;
    std::string input="";
    while (true)
    {
        std::cout <<"Enter your expression or Q to exit: ";
        std::getline (std::cin, input);
       
        if ( input == "Q" || input == "q")
            break;

        word_calculator.Calculate(input);
    }    

    std::cout <<"Bye!"<<std::endl;
    return 0;

}